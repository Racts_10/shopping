/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sheridan;

/**
 *
 * @author Rachit Vyas
 */
public class DiscountByPercentage extends Discount 
{
        public DiscountByPercentage(){}

    @Override
    public double getPrice() 
    {
        return super.getPrice(); 
    }

    @Override
    public double CalculateDiscount(double amount) 
    {
        return super.CalculateDiscount(amount) - super.getPrice() / 100; 
    }

 }
