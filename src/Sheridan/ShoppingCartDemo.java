/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sheridan;

/**
 *
 * @author Rachit Vyas
 */
public class ShoppingCartDemo {
    
    public static void main( String args [ ] ) {
        PaymentServiceFactory factory = PaymentServiceFactory.getInstance( );
        PaymentService creditService = factory.getPaymentService( PaymentServiceType.CREDIT );
        PaymentService debitService = factory.getPaymentService( PaymentServiceType.DEBIT );        
            // create cart and add products
        Cart cart = new Cart( );
        cart.addProduct( new Product( "Tshirt" , 35 ) );
        cart.addProduct( new Product( "Fruits" , 70 ) );
            // set credit service and pay
        cart.setPaymentService( creditService );        
        cart.payCart();
            // set debit service and pay
        cart.setPaymentService( debitService );    
        cart.payCart();        
        
    } 
    private static Boolean checkProduct(String product)
        {
                return true;
        }

    public static Boolean validateProduct(String product)
        {
               return false;
        }
    
}